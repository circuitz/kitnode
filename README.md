# model-kitnode

## NodeMCU Setup

```bash
$ python esptool.py  --port /dev/ttyUSB0 --baud 115200 write_flash --verify --flash_freq 80m --flash_mode dio --flash_size 32m 0x0000 "boot_v1.4(b1).bin" 0x1000 espruino_esp8266_user1.bin 0x37E000 blank.bin
```

```
Directory Structure
.
├── binary
├── boot.js
├── firmware
├── flash.sh
├── modules
│   ├── config.js
│   ├── debug.js
│   ├── log.js
│   ├── main.js
│   ├── network.js
│   ├── routeGetStatus.js
│   └── routes
├── package.json
├── package-lock.json
├── projects
├── README.md
├── scripts
├── snippets
│   └── terminalsnippets.txt
├── stm32loader.py
├── testing
└── testinglog
```
