#!/bin/bash

# Script used to flash a NodeMCU board with Espruino JS environment.

ESP_DIR=/home/peter/Development/esptool
ESPTOOL_DIR=${ESP_DIR}/esptool.py

ESP_PORT=/dev/ttyUSB0
ESP_BAUD=115200

ESP_BIN_DIR=${ESP_DIR}/2v04_esp8266_4mb
ESP_BLANK_BIN=${ESP_BIN_DIR}/blank.bin
ESP_BOOT_BIN=${ESP_BIN_DIR}/boot_v1.6.bin
ESP_INIT_BIN=${ESP_BIN_DIR}/esp_init_data_default.bin
ESP_USER1_BIN=${ESP_BIN_DIR}/espruino_esp8266_user1.bin
ESP_USER2_BIN=${ESP_BIN_DIR}/espruino_esp8266_user2.bin

python ${ESPTOOL_DIR} \
	--port ${ESP_PORT} \
	--baud ${ESP_BAUD} \
	write_flash \
	--verify \
	--flash_freq 80m \
	--flash_mode qio \
	--flash_size 32m \
	0x000 ${ESP_BOOT_BIN} \
	0x1000 ${ESP_USER1_BIN} \
	0x3FC000 ${ESP_INIT_BIN} \
	0x3FE000 ${ESP_BLANK_BIN}
