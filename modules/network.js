'use strict';

var wifi = require('Wifi');
var ws = require('ws');

var PORT = 8777;
var clients = [];
var routes = {};
var config = require('config').network;

/**
 * Initialize the NodeMCU network. Bootstraps Wifi and HTTP server.
 */
function init() {
  console.log('network.init()');

  startWifi(config.ssid, config.options);

  startWebServer();

  addRoute('GET', '/status', require('routeGetStatus'));
}

/**
 * Start the wireless interface.
 */
function startWifi(ssid, options) {
  console.log('Connecting to SSID ' + ssid + '...');
  wifi.connect(ssid, options, function(err) {
    if (err) {
      console.log(err);
      throw err;
    }

    console.log('Connected to SSID ' + ssid);
    console.log(wifi.getStatus());

    wifi.getIP(function(info) {
      console.log(info);
      if (!info) return;
      console.log('http://' + info.ip + ':' + PORT);
    });
  });
}

function startWebServer() {
  console.log('Starting web server on port ' + PORT);
  var s = ws.createServer(onRequest);
  s.on('websocket', onWebsocket);
  s.listen(PORT);
  console.log('Listening on :' + PORT);
}

function onRequest(req, res) {
  console.log(req.method + ' ' + req.url);
  var result;
  var response = {
    code: 500
  };
  var code = 500;

  req.on('error', function(e) {
    console.log(e);
  });

  // Not found
  if (!routes.hasOwnProperty(req.url)) {
    res.writeHead(404, { 'Content-Type': 'text/plain' });
    res.end();
    return;
  }

  // Method not allowed
  if (!routes[req.url].hasOwnProperty(req.method)) {
    res.writeHead(405, { 'Content-Type': 'text/plain' });
    res.end();
    return;
  }

  try {
    result = routes[req.url][req.method](req, res);
    result = JSON.stringify(result);
    res.writeHead(200, { 'Content-Type': 'application/json', 'Content-Length': result.length });
    res.write(result);
    res.end();
  } catch (e) {
    console.log('Error: ' + e.message);
    res.writeHead(500, {
      'Content-Type': 'text/html'
    });
    res.end();
  }
}

function onWebsocket(ws) {
  clients.push(ws);
  ws.on('message', onWebsocketMessage);
  ws.on('close', onWebsocketClose);
}

function onWebsocketMessage(msg) {
  console.log(msg);
}

function onWebsocketClose(evt) {
  console.log(evt);
}

function addRoute(method, url, fn) {
  if (!routes.hasOwnProperty(url)) {
    routes[url] = {};
  }

  if (!routes[url].hasOwnProperty(method)) {
    routes[url][method] = fn;
  }
}

module.exports = {
  init: init
};
