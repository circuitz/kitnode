'use strict';

var DEBUG = false;

module.exports.init = function() {
  if (DEBUG === true) {
    require('debug').debug();
    return;
  }

  var network = require('network');
  console.log('main.init()');
  network.init();
};
