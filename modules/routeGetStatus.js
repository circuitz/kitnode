module.exports = function(req, res) {
  return {
    time: getTime(),
    serial: getSerial()
  };
};
