'use strict';

var wifi = require('Wifi');


/**
 * Blink a given pin on a specified delay.
 * @param {number} pin The pin to blink.
 * @param {number} delay The number of milliseconds between on- and off-states.
 */
function blinkPin(pin, delay) {
  var on = 0;
  digitalWrite(pin, on);
  setInterval(function() {
    on = on === 0 ? 1 : 0;
    digitalWrite(pin, on);
  }, delay);
}

function blinkAnalogPin(pin, delay) {
  var on = 0;
  analogWrite(pin, 0.5, { soft: true });
  setInterval(function() {
    on = on === 0 ? 1 : 0;
    analogWrite(pin, 0, { soft: true });
  }, delay);
}

function checkPins() {
  var pins = [
    'A0',
    'D0',
    'D1',
    'D2',
    'D3',
    'D4',
    'D5',
    'D6',
    'D7',
    'D8',
    'D9',
    'D10'
  ];

  var pin = 0;
  var i = 0;
  var pinInfo = null;

  for (i = 0; i < pins.length; i++) {
    try {
      var pinName = 'D' + i;
      pin = NodeMCU[pinName];
      pinInfo = pin.getInfo();
      console.log('-------------------------');
      console.log('Pin name: ' + pinName);
      console.log(pinInfo);
    } catch (e) {
      console.log(e.message);
    }
  }
}

module.exports = {
  debug: function() {
    var delay = 300;

    // blinkPin(NodeMCU.D0, delay);
    // blinkPin(NodeMCU.D1, delay);
    // blinkPin(NodeMCU.D2, delay);
    // blinkPin(NodeMCU.D3, delay);
    // blinkPin(NodeMCU.D4, delay);
    // blinkPin(NodeMCU.D5, delay);
    // blinkPin(NodeMCU.D6, delay);
    // blinkPin(NodeMCU.D7, delay);
    // blinkPin(NodeMCU.D8, delay);
    // blinkPin(NodeMCU.D9, delay);
    // blinkPin(NodeMCU.D10, delay);
    // checkPins();

    console.log(wifi.macAddress());

  }
};
